const mysql = require("mysql");
const SETTINGS = require("../configs/databaseConfig").mysql;

const connection = mysql.createConnection(SETTINGS);

module.exports = class Database {
  static connect(callback) {
    connection.connect(err => {
      if (err) {
        console.log(`Connection to database failed!`, err);
        callback(false);
      }
      console.log(`Database connected on port: ${SETTINGS.host}`);
      callback(true);
    });
  }

  static executeCommand(query, callback) {
    connection.query(query, (err, res) => {
      if (err) {
        callback(false);
      }
      callback(res);
    });
  }
};
