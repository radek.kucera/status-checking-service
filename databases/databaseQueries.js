const TABLES = require("../configs/databaseTablesConfig");
const RESULT_LIMIT = require("../configs/api/monitoring/requestSendingConfig")
  .numberOfLastResults;

module.exports.user = accessToken => {
  return {
    read:
      "SELECT * FROM `" +
      TABLES.users +
      "` WHERE `accessToken` LIKE '" +
      accessToken +
      "'"
  };
};

module.exports.result = {
  create: res => {
    let values = `('${res.id}', '${res.endPointId}', '${res.checkDate}', '${res.httpStatusCode}', '${res.httpPayload}')`;
    return (
      "INSERT INTO `" +
      TABLES.results +
      "` (id, endPointId, checkedDate, httpStatusCode, httpPayload) VALUES" +
      values
    );
  },
  readFewLast: endPointId => {
    return (
      "SELECT * FROM `" +
      TABLES.results +
      "` WHERE endPointId='" +
      endPointId +
      "' ORDER BY checkedDate DESC LIMIT " +
      RESULT_LIMIT
    );
  }
};

module.exports.endPointMenaging = {
  create: endPoint => {
    let values = `('${endPoint.id}', '${endPoint.name}', '${endPoint.url}', '${endPoint.ownerAccessToken}', '${endPoint.createdDate}')`;
    return (
      "INSERT INTO `" +
      TABLES.endPoints +
      "` (id, name, url, ownerAccessToken, createdDate) VALUES" +
      values
    );
  },
  read: ownerAccessToken => {
    return (
      "SELECT * FROM `" +
      TABLES.endPoints +
      "` WHERE `ownerAccessToken` LIKE '" +
      ownerAccessToken +
      "'"
    );
  },
  readById: endPointId => {
    return (
      "SELECT * FROM `" +
      TABLES.endPoints +
      "` WHERE `id` LIKE '" +
      endPointId +
      "'"
    );
  },
  readAll: () => {
    return "SELECT id, url FROM `" + TABLES.endPoints + "`";
  },
  update: data => {
    let name = data.name ? "name='" + data.name + "', " : "";
    let url = data.url ? "url='" + data.url + "', " : "";
    let ownerAccessToken = data.ownerAccessToken
      ? "ownerAccessToken='" + data.ownerAccessToken + "', "
      : "";

    let query =
      "UPDATE `" +
      TABLES.endPoints +
      "` SET " +
      name +
      url +
      ownerAccessToken;
    return query.slice(0, -2) + " WHERE id='" + data.id + "'";
  },
  delete: (accessToken, id) => {
    return (
      "DELETE FROM `" +
      TABLES.endPoints +
      "` WHERE id='" +
      id +
      "' AND ownerAccessToken='" +
      accessToken +
      "'"
    );
  }
};
