module.exports = server => {
  server.post("/monitor", require("./api/monitoring"));
  server.post("/manage", require("./api/managing"));
  server.get("/", (req, res) => {
    res.json(
      JSON.stringify({
        code:
          "Service is running. Try /api/manage and /api/monitoring to continue!"
      })
    );
  });
};
