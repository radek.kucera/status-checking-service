const ResultManager = require("../../controllers/resultManager");

module.exports = (req, res) => {
  if (req.headers.authorization && req.body) {
    ResultManager.read(req.headers.authorization, req.body, data => {
      res.send(data);
    });
  } else res.send(JSON.stringify({ status: false, data: {} }));
};
