const EndPointManager = require("../../controllers/endPointManager");
const FAILED = JSON.stringify({ status: false, data: {} });

module.exports = (req, res) => {
  if (req.headers.authorization && req.query.type) {
    switch (req.query.type) {
      case "create":
        if (req.body) {
          console.log("YOu");
          EndPointManager.create(req.headers.authorization, req.body, data => {
            res.send(data);
          });
        } else res.send(FAILED);
        break;

      case "read":
        EndPointManager.read(req.headers.authorization, data => {
          res.send(data);
        });
        break;

      case "update":
        if (req.body) {
          EndPointManager.update(req.headers.authorization, req.body, data => {
            res.send(data);
          });
        } else res.send(FAILED);
        break;

      case "delete":
        if (req.body) {
          EndPointManager.delete(req.headers.authorization, req.body, data => {
            res.send(data);
          });
        } else res.send(FAILED);
        break;

      default:
        res.send(FAILED);
        break;
    }
  } else res.send(FAILED);
};
