const actions = require("../models/stores/endPoints");
const isUrlValid = require("../models/requestSender").urlIsValid;

const FAILED = { status: false, data: {} };
/**
 * Class for managing endpoints.
 */
class EndPointManager {
  /**
   * Method for creating new endpoint.
   * @param  {String} accessToken - The string containing unique access token to endpoints.
   * @param  {Object} body - Object representing request body.
   * @param  {JSON} callback - Stringified object with information about status and containing response data.
   */
  static create(accessToken, body, callback) {
    let promise = new Promise((resolve, reject) => {
      if (
        body.name &&
        body.url &&
        typeof body.name == "string" &&
        typeof body.url == "string" &&
        typeof accessToken == "string"
      ) {
        isUrlValid(body.url, isValid => {
          if (isValid) {
            actions.create(body.name, body.url, accessToken, res => {
              console.log("EndPoint creation status: ", !!res);
              res ? resolve({ status: true, data: res }) : resolve(FAILED);
            });
          } else resolve(FAILED);
        });
      } else {
        console.log("EndPoint creation status: ", false);
        resolve(FAILED);
      }
    });

    promise.then(res => callback(JSON.stringify(res)));
  }

  /**
   * Method for reading endpoints.
   * @param  {String} accessToken - The string containing unique access token to endpoints.
   * @return {JSON} - Stringified object with information about status and containing response data.
   */
  static read(accessToken, callback) {
    let promise = new Promise((resolve, reject) => {
      if (typeof accessToken == "string") {
        actions.read(accessToken, res => {
          console.log("EndPoint reading status: ", !!res);
          res ? resolve({ status: true, data: res }) : resolve(FAILED);
        });
      } else {
        console.log("EndPoint reading status: ", false);
        resolve(FAILED);
      }
    });

    promise.then(res => callback(JSON.stringify(res)));
  }

  /**
   * Method for updating already created endpoints.
   * @param  {String} accessToken - The string containing unique access token to endpoints.
   * @param  {Object} body - Object representing request body.
   * @param  {JSON} callback - Stringified object with information about status and containing response data.
   */
  static update(accessToken, body, callback) {
    let promise = new Promise((resolve, reject) => {
      if (
        typeof accessToken == "string" &&
        body.id &&
        typeof body.id == "string"
      ) {
        if (body.url) {
          isUrlValid(body.url, isValid => {
            if (isValid) {
              actions.update(accessToken, body.id, body, res => {
                console.log("EndPoint updating status: ", !!res);
                res ? resolve({ status: true, data: res }) : resolve(FAILED);
              });
            } else resolve(FAILED);
          });
        } else {
          actions.update(accessToken, body.id, body, res => {
            console.log("EndPoint updating status: ", !!res);
            res ? resolve({ status: true, data: res }) : resolve(FAILED);
          });
        }
      } else resolve(FAILED);
    });

    promise.then(res => callback(JSON.stringify(res)));
  }

  /**
   * Method for deleting already created endpoints.
   * @param  {String} accessToken - The string containing unique access token to endpoints.
   * @param  {Object} body - Object representing request body.
   * @param  {JSON} callback - Stringified object with information about status and containing response data.
   */
  static delete(accessToken, body, callback) {
    let promise = new Promise((resolve, reject) => {
      if (
        body.id &&
        typeof accessToken == "string" &&
        typeof body.id == "string"
      ) {
        actions.delete(accessToken, body.id, res => {
          console.log("EndPoint deleting status: ", !!res);
          res ? resolve({ status: true, data: res }) : resolve(FAILED);
        });
      } else resolve(FAILED);
    });

    promise.then(res => callback(JSON.stringify(res)));
  }
}

/**
 * Module representing class for managing endpoints with CRUD.
 * @module controllers/endPointManager
 */
module.exports = EndPointManager;
