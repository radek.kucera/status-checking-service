const results = require("../models/stores/results");

/**
 * Class for managing monitoring results.
 */
class ResultManager {
  /**
   * @param  {String} accessToken - The string containing unique access token to endpoints.
   * @param  {Object} body - Object representing request body with id representing endPointId.
   * @param  {JSON} callback - Stringified object with information about status and containing response data.
   */
  static read(accessToken, body, callback) {
    if (typeof accessToken == "string" && typeof body.id == "string") {
      results.read(body.id, accessToken, res => {
        console.log("EndPoint results reading status: ", !!res);
        res
          ? callback(JSON.stringify({ status: true, data: res }))
          : callback(JSON.stringify({ status: false, data: {} }));
      });
    } else callback(JSON.stringify({ status: true, data: res }));
  }
}

/**
 * Module representing class for managing monitoring results. (Actually only reading)
 * @module controllers/resultManager
 */
module.exports = ResultManager;
