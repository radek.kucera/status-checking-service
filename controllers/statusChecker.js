const db = require("../databases/database");
const dbQuery = require("../databases/databaseQueries");
const requestSender = require("../models/requestSender");
const ResultSchema = require("../models/schemas/monitoringResultSchema");

/**
 * Class for sending http request to urls of endpoints,
 */
class StatusCheckingService {
  /**
   * Method for sending requests to urls of all endpoints in database.
   * @param  {Function} callback - Operation status information.
   */
  static checkAllEndPoints(callback) {
    let promise = new Promise((resolve, reject) => {
      db.executeCommand(dbQuery.endPointMenaging.readAll(), res => {
        if (res.length > 0) resolve(res);
        else resolve(false);
      });
    });

    promise.then(res => {
      if (res) {
        let array = [];
        requestSender.checkAll(res, results => {
          results.forEach(item => {
            array.push(
              new ResultSchema(
                res[results.indexOf(item)].id,
                item.statusCode,
                item.description
              ).toJson()
            );
          });
          callback(array);
        });
      } else callback(false);
    });
  }
}

/**
 * Module representing class for sending http request to urls of endpoints
 * @module controllers/statusChecker
 */
module.exports = StatusCheckingService;
