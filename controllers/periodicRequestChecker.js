const requestSender = require("../controllers/statusChecker");
const SETTINGS = require("../configs/api/monitoring/requestSendingConfig.json");

module.exports = () => {
  setInterval(() => {
    requestSender.checkAllEndPoints(res => {
      console.log(
        "Periodic control status: ",
        !!res,
        ", ",
        new Date().toLocaleString()
      );
    });
  }, SETTINGS.period);
};
