const restify = require("restify");
const SETTINGS = require("./configs/serverConfig");
const Database = require("./databases/database");
const corsMiddleware = require("restify-cors-middleware");

const cors = corsMiddleware({
  origins: ["*"],
  allowHeaders: ["authorization", "Content-Type"]
});

module.exports.run = () => {
  new Promise((resolve, reject) => {
    Database.connect(res => {
      resolve(res);
    });
  })
    .then(status => {
      console.log("Database status: ", status);
      if (status) {
        const server = restify.createServer();
        server.use(restify.plugins.queryParser());
        server.use(restify.plugins.bodyParser());
        server.pre(cors.preflight);
        server.use(cors.actual);

        server.listen(SETTINGS.port, () => {
          console.log(SETTINGS.hostMessage, SETTINGS.port);
        });

        console.log("Server status: ", true);
        return server;
      } else {
        console.log("Please connect database!");
        return false;
      }
    })
    .then(server => {
      if (server) {
        require("./routes/router")(server);

        require("./controllers/periodicRequestChecker")();
      }
    });
};
