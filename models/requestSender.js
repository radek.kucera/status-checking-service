const sc = require("status-check");
const urlExists = require("url-exists");

module.exports.urlIsValid = (url, callback) => {
  urlExists(url, (err, exists) => {
    callback(!!exists);
  });
};

module.exports.checkAll = (arrayOfURLs, callback) => {
  let filteredURLList = [];
  arrayOfURLs.forEach(col => {
    filteredURLList.push(col.url);
  });

  sc.startCheckingLink(filteredURLList, res => {
    callback(res);
  });
};
