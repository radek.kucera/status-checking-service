const db = require("../../databases/database");
const dbQueries = require("../../databases/databaseQueries");
const EndPointSchema = require("../schemas/monitoredEndPointSchema");

module.exports = class EndPoints {
  static create(name, url, ownerAccessToken, callback) {
    let endPoint = new EndPointSchema(name, url, ownerAccessToken);

    let userEndPoints = new Promise((resolve, reject) => {
      EndPoints.read(ownerAccessToken, res => {
        resolve(res);
      });
    });

    let status = true;

    userEndPoints
      .then(table => {
        table.forEach(item => {
          if (item.name === endPoint.name) {
            status = false;
          }
        });
      })
      .then(() => {
        if (status) {
          db.executeCommand(
            dbQueries.endPointMenaging.create(endPoint),
            res => {
              callback(res);
            }
          );
        } else callback(false);
      });
  }

  static read(ownerAccessToken, callback) {
    db.executeCommand(
      dbQueries.endPointMenaging.read(ownerAccessToken),
      res => {
        callback(res);
      }
    );
  }

  static update(accessToken, id, data, callback) {
    if (
      data.name ||
      data.url ||
      data.ownerAccessToken
    ) {
      let endPoint = new Promise((resolve, reject) => {
        db.executeCommand(dbQueries.endPointMenaging.readById(id), res => {
          resolve(res);
        });
      });

      endPoint.then(table => {
        if (table && table.length > 0) {
          let result = table[0];

          if (result.ownerAccessToken === accessToken) {
            db.executeCommand(dbQueries.endPointMenaging.update(data), res => {
              callback(res);
            });
          }
        }
      });
    } else callback(false);
  }

  static delete(accessToken, id, callback) {
    db.executeCommand(
      dbQueries.endPointMenaging.delete(accessToken, id),
      res => {
        callback(res);
      }
    );
  }
};
