const db = require("../../databases/database");
const dbQueries = require("../../databases/databaseQueries");

module.exports = class Users {
  static getUserByAccessToken(accessToken, callback) {
    db.executeCommand(dbQueries.user(accessToken).read, res => {
      callback(res);
    });
  }
};
