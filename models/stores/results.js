const database = require("../../databases/database");
const dbQueries = require("../../databases/databaseQueries");

module.exports = class Results {
  static create(result, callback) {
    database.executeCommand(dbQueries.result.create(result), res => {
      callback(res);
    });
  }

  static read(endPointId, accessToken, callback) {
    let promise = new Promise((resolve, reject) => {
      database.executeCommand(
        dbQueries.endPointMenaging.read(accessToken),
        res => {
          if (res) {
            res.forEach(col => {
              if (col.id === endPointId) {
                resolve(col);
              }
            });
            resolve(false);
          }
          resolve(false);
        }
      );
    });

    promise.then(res => {
      if (res) {
        database.executeCommand(
          dbQueries.result.readFewLast(endPointId),
          data => {
            if (data) callback(data);
            else callback(false);
          }
        );
      } else {
        callback(false);
      }
    });
  }
};
