const jwt = require("jsonwebtoken");
const SETTINGS = require("../../configs/api/managing/resultConfig")
  .monitoringResult;

const results = require("../stores/results");

module.exports = class MonitoringResultSchema {
  constructor(endPointId, statusCode, statusPayload) {
    this.id = this._createId();
    this.endPointId = endPointId;
    this.checkDate = new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ");
    this.httpStatusCode = statusCode;
    this.httpPayload = statusPayload;
    this._createResult();
  }

  _createResult() {
    results.create(this, res => {
      if (!res)
        console.log(
          "Periodic control error at: ",
          this.endPointId,
          this.checkDate
        );
    });
  }

  _createId() {
    return jwt.sign(
      {
        endPointId: this.endPointId,
        checkDate: this.checkDate
      },
      SETTINGS.secret
    );
  }

  toJson() {
    return JSON.stringify({
      id: this.id,
      endPointId: this.endPointId,
      checkDate: this.checkDate,
      httpStatusCode: this.httpStatusCode,
      httpPayload: this.httpPayload
    });
  }
};
