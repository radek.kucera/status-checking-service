const jwt = require("jsonwebtoken");
const SETTINGS = require("../../configs/api/managing/endPointConfig")
  .monitoredEndPoint;

module.exports = class MonitoredEndPointSchema {
  constructor(name, url, ownerAccessToken) {
    this.name = name;
    this.url = url;
    this.ownerAccessToken = ownerAccessToken;
    this.createdDate = new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ");
    this.id = this._createId();
  }

  _createId() {
    return jwt.sign(
      {
        ownerAccessToken: this.ownerAccessToken,
        endPointName: this.name
      },
      SETTINGS.secret
    );
  }
};
