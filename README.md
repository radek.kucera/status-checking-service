# STATUS-CHECKING-SERVICE

- **Author**: Radek Kucera - 12. 11. 2019

### Description

REST API JSON Node.js microservice, used for monitoring URL adresses and managing user EndPoints.

### Needed technologies

- **NodeJS** - Project technology.
- **MySQL** - Database - Eventually can be remote.

### How to run

```sh
$ cd status-checking-service
$ npm install
$ npm start
```

# Routes

### /

Route for checking service status.

**Method** - GET

#### Response:

    { "code": some_status_text }

### /monitor

Route for checking user EndPoints request results by AccessToken. Responsing last 10 results.

**Method** - POST

#### Headers:

    application/json
    authorization: user_access_token

#### Request body:

    { "id": end_point_id }

### /manage

Route for managing user EndPoints.

**Method** - POST

#### Queries:

**Type** - ?type=your_type

- your_types - create, read, update, delete

#### Headers:

    application/json
    authorization: user_access_token

#### Request body:

**read**

    no body required

**update**

    { "id": end_point_id,  "name": ?end_point_name, "url": ?end_point_url, "ownerAccessToken": ?new_owner_access_token }

**create**

    { "name": end_point_name, "url": end_point_url }

**delete**

    { "id": end_point_id }
